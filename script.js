$(function() {

  var varH = 0;
  var varM = 0;
  var varS = 0;
  var s = "0" + varS;
  var m = "0" + varM;
  var h = "0" + varH;
  var session = "AM";
  var time = "00:00:00 AM";

  function showTime(){
      varS++;
      if (varS > 9 && varS < 59 ) {
        s = varS;
      }
      if (varS >= 59) {
        varM++;
        varS = 0;
        s = "0" + varS;
      }
      if (varM > 9 && varM < 59) {
        m = varM;
      }
      if (varM > 59) {
        varM = 0;
        m = "0" + varM;
        varH++;
      }
      if (varH > 9) {
        h = varH;
      }
      if (varH > 23) {
        varH = 0;
        h = "0" + varH;
      }

  }

  var time = h + ":" + m + ":" + s + " " + session;
  document.getElementById("MyClockDisplay").innerHTML = time;
});
