window.onload = () => {
  const gui = new dat.GUI();
  var socket = io();

  
  var control = { number: 1, wanderingIntensity: 10, speed: 3 };
  
  var personNumber = gui.add(control, 'number', 1, 1000);
  var wandering = gui.add(control, 'wanderingIntensity', 0, 10);
  
  var speed = gui.add(control, 'speed', 3, 6);

  personNumber.onChange(() => {
    socket.emit('change', control)
  });
  
  wandering.onChange(() => {
    socket.emit('change', control)
  });

  speed.onChange(() => {
    socket.emit('change', control)
  });
}
