window.onload = () => {
  var socket = io();
  var control = { number: 1, wanderingIntensity: 10, speed: 3, isPlaying: false, play: function(){this.isPlaying = !this.isPlaying} };
  socket.on('change', object => {
    control = object
  });
  var song;
  var persons = [];
  var zones = [];
  var pg;
  var widthRect = 50;
  var heightRect = 50;
  const prepareZonesIndexes = (random) => {
    let zonesTab = [];
    for (let y = 0; y < parseInt(random); y++) {
      zonesTab.push(y);
    }
    return zonesTab;
  }
  
  class Zone {
    constructor(x, y, zoneWidth, zoneHeight) {
      this.pos = createVector(x, y);
      this.zoneWidth = zoneWidth;
      this.zoneHeight = zoneHeight;
      this.color = color('#FFECAA');
      this.stroke = color('#FFECAA')
      this.impact = createVector(this.pos.x, this.pos.y);
    }
    draw() {
      fill(this.color);
      stroke(this.stroke);
      rect(this.pos.x, this.pos.y, this.zoneWidth, this.zoneHeight);
    }
    grow() {
      if (this.zoneWidth <= 200) {
        // var size = 25/control.number
        // this.zoneWidth += size;
        // this.zoneHeight += size;
        this.zoneWidth += 1;
        this.zoneHeight += 1;
      }
    }
    reduce() {
      // var size = 25/control.number
      // this.zoneWidth -= size;
      // this.zoneHeight -= size;
      this.zoneWidth -= 1;
      this.zoneHeight -= 1;
    }
  }

  class NewPerson {
    constructor() {
      this.size = 10;
      this.pos = createVector(random(0, width), random(0, height));
      this.color = color('#A69A45');
      this.zonesTab = prepareZonesIndexes(parseInt(random(6, 8)));
      this.zone = zones[parseInt(random(0, this.zonesTab.length))];
      this.vit = createVector(0);
      this.accel = createVector(0);
      this.accelImpact = createVector(0);
      this.isInZone = false;
      this.isDead = false;
      this.timer = 0;
    }

    draw() {
      pg.fill(this.color);
      pg.push();
      pg.ellipse(this.pos.x, this.pos.y, this.size);
      pg.pop();
    }

    update() {
      this.accelImpact = createVector(random(this.pos.x, this.zone.impact.x), random(this.pos.y, this.zone.impact.y)).sub(this.pos).normalize().mult(0.2);
      if(control.wanderingIntensity !== 0){
      if (frameCount > 0 && frameCount % 20 == 0) {
        this.accel = createVector(random(-0.1, 0.1), random(-0.1, 0.1)).mult(parseInt(random(0, control.wanderingIntensity)));
      }
      if (this.vit.mag() < (parseInt(control.speed)/2)) {
        this.vit.add(this.accel.copy().add(this.accelImpact));
      } else {
        this.vit.normalize().mult((parseInt(control.speed)/2) - 0.1);
      }
    }else{
      this.vit = this.zone.impact.copy().sub(this.pos).normalize().mult((parseInt(control.speed)/2));
    }

      if (this.isInZone) {
        this.timer += 1
      }

      this.pos.add(this.vit);

      let distImpact = this.pos.dist(this.zone.impact);
      if (distImpact < 5) {
        this.isInZone = true;

        if (this.timer === 200) {
          this.zone.reduce();
          this.zone = zones[parseInt(random(parseInt(random(0,7)), this.zonesTab.length))];
          this.timer = 0;
          this.isInZone = false;
          this.zone.grow();
        }
 
      }
      else if (distImpact < 100) {
        this.accel = createVector(0);
        this.vit = this.zone.impact.copy().sub(this.pos).normalize().mult(1.5);
      }
    }
  }

  class RegularPerson extends NewPerson {
    constructor() {
      super();
      this.color = color('#B3C089');
      this.stroke = color('#3A4067');
      this.zonesTab = prepareZonesIndexes(parseInt(random(2, 4)));
      this.vit = this.zone.impact.copy().sub(this.pos).normalize().mult(control.speed);
      this.accel = createVector(0);
      this.accelImpact = createVector(0);
    }

    draw() {
      pg.push();
      pg.fill(this.color);
      pg.stroke(this.stroke);
      pg.ellipse(this.pos.x, this.pos.y, this.size);
      pg.pop();
    }

    update() {
      this.pos.add(this.vit);

      if (this.isInZone) {
        this.timer += 1
      }
      this.vit = this.zone.impact.copy().sub(this.pos).normalize().mult(control.speed);
      if (this.pos.dist(this.zone.impact) < 5) {

        this.isInZone = true;
        if (this.timer === 200) {
          this.zone.reduce();
          this.zone = zones[parseInt(random(parseInt(random(0,7)), this.zonesTab.length))];
          this.timer = 0;
          this.isInZone = false;
          this.zone.grow();
        }
        this.vit = this.zone.impact.copy().sub(this.pos).normalize().mult(control.speed);
      }
    }
  }

  preload = () => {
    song = loadSound('./edvard_grieg.mp3');
  }
  setup = () => {
    colorMode(HSB, 100);
    pg = createGraphics(windowWidth, windowHeight);
    createCanvas(windowWidth, windowHeight);
    rectMode(CENTER);

    song.play();
    
    // Crée un certain nombre de zones
    for (let i = 0; i < 8; i++) {
      zones.push(new Zone(random(widthRect, width - widthRect), random(heightRect, height - heightRect), widthRect, heightRect));
    }
    persons.push(new NewPerson());

    // Crée un certain nombre de personnes
    for (let i = 0; i < control.number; i++) {
      let rand = random(0, 1);
      if (rand > 0.5) {
        persons.push(new NewPerson());
      }
      else {
        persons.push(new RegularPerson());
      }
    }
  }

  draw = () => {
  // var speed = map(control.speed*100, 0.1, height, 0, 2);
  // speed = constrain(speed, 0.01, 4);
  // song.rate(speed);

    image(pg, 0, 0);
    if (frameCount%3==0){
      pg.background(0, 10);
    }

    while (control.number > persons.length) {
      let personTypeBool = random([true, false]);

      if (personTypeBool) {
        persons.push(new NewPerson());
      }
      else {
        persons.push(new RegularPerson());
      }
    }

    while (control.number < persons.length) {

      let p = persons.pop();
      // p.zone.reduce();
    }

    for (let i = 0; i < persons.length; i++) {
      persons[i].update();
      if(control.wanderingIntensity == 0){
        if(Object.getPrototypeOf(persons[i])==NewPerson.prototype){
          persons[i].color = color('#B3C089');
          persons[i].stroke = color('#3A4067');
        }
      }
    }

    for (let i = 0; i < persons.length; i++) {
      persons[i].draw();
    }

    for (let i = 0; i < zones.length; i++) {
      zones[i].draw();
    }
  }
}
