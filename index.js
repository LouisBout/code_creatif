const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);


io.on('connection', function(socket){
  socket.on('change', value => {
    io.emit('change', value)
  });
});


app.use(express.static(__dirname));

app.get('/', (req, res) => {
  res.sendFile(__dirname+'/sketch.html')
})

app.get('/controller', (req, res) => {
  res.sendFile(__dirname+'/controller.html')
})

http.listen(3000, function(){
  console.log('listening on *:3000');
});